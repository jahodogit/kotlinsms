interface UseCase<T> {

    fun execute(): T

}