
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
class Sms {
    @Autowired
    private lateinit var enviarSms: EnviarSms

    @RequestMapping("apisms/enviar",method = arrayOf(RequestMethod.POST))
    fun enviarMensaje(@RequestBody dtoMensaje: DTOMensaje): Boolean{
        enviarSms.setDatos(dtoMensaje)
        return enviarSms.execute()
    }

}
