import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class EnviarSms(@Autowired val mensajeRepository: IMensajeRepository) : UseCase<Boolean>{



    override fun execute(): Boolean {
        return mensajeRepository.create(this.dtoMensaje)
        //val ConexionSMS = ConexionSMS("3187287882","Hola enviado desde kotlin api")
        return true
    }

    private lateinit var dtoMensaje: DTOMensaje


    fun setDatos(dtoMensaje: DTOMensaje){
        this.dtoMensaje = dtoMensaje

    }
}