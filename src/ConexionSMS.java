
import com.google.gson.Gson;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.Mac;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.codec.digest.HmacUtils;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author juan.pinzon
 */

public class ConexionSMS {

  //Apikey asignado por la cuenta
  final static String _APIKEY = "1269-2585-5ba5300ec17ce1.45944621";
  //Secret asignado por la cuenta
  final static String _SECRET = "7a39f4785321171be2b8e22af12f3c468e91fd13";
  //Url donde se hace la petición para hacer los envíos de SMS encriptados
  final static String _URLAPI = "https://aio.sigmamovil.com/api/sms/createsmsencrypted";
  //Url para probar la conexión
  //final static String _URLAPI = "https://aio.sigmamovil.com/api/v1/echo";
  //Métodos por el cual se hace la petición
  //final static String _METHODGET = "GET";
  final static String _METHODPOST = "POST";
  //Data que sirve para probar la conexión
  final static String _data = "";

  //Método de ejecución de Java
  public ConexionSMS(String numero, String mensaje) {
    
//    System.out.println("Me acabas de pasar " + args + " parametros");
    
    try {
      //Método que desactiva el SSL (No recomendado en entornos de producción)
      prepareCone();

      //Llamado del método que hace la conexión con el Api y recibe la información para el envío de SMS encriptado
      connection(_APIKEY, _SECRET, _URLAPI, _METHODPOST, Base64.encodeBase64String(sendEncripted(numero,mensaje).getBytes()));

    } catch (Exception ex) {
      System.out.println(ex.getMessage());
    }
  }

  public static void prepareCone() {
    try {
      TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
          return null;
        }

        public void checkClientTrusted(X509Certificate[] certs, String authType) {
        }

        public void checkServerTrusted(X509Certificate[] certs, String authType) {
        }
      }
      };

      // Install the all-trusting trust manager
      SSLContext sc = SSLContext.getInstance("SSL");
      sc.init(null, trustAllCerts, new java.security.SecureRandom());
      HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

      // Create all-trusting host name verifier
      HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
          return true;
        }
      };

      HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Método que hace la conexión con el Api y recibe la información para el
   * envío de SMS encriptado
   *
   * @param apikey
   * @param secret
   * @param urlApi
   * @param method
   * @param data
   * @throws IOException
   */
  public static void connection(String apikey, String secret, String urlApi, String method, String data) throws IOException {
    //Método de encriptación al algoritmo Hmac, se usaron las librerías de apache commons codec
    HmacUtils hmac = new HmacUtils();
    Mac mac = hmac.getHmacSha1(secret.getBytes());
    String text = method + "|" + urlApi + "|" + data;

    String pwd = Hex.encodeHexString(mac.doFinal(text.getBytes()));

    String pwds = apikey + ":" + pwd;
    
    //Conexión al api y envio de la información del SMS
    URL url = new URL(urlApi);
    HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
    con.setRequestProperty("Authorization", "Hmac " + Base64.encodeBase64String(pwds.getBytes()));
    con.setRequestMethod(method);
    con.setDoOutput(true);
    con.getOutputStream().write(data.getBytes());

    //Se lee e imprime la información que responde el api
    Reader reader = new InputStreamReader(con.getInputStream());
    while (true) {
      int ch = reader.read();
      if (ch == -1) {
        break;
      }
      System.out.print((char) ch);
    }
  }

  /**
   * Método que construye el JSON con la información del envío más los
   * destinatarios
   *
   * @return String
   */
  public static String sendEncripted(String numero, String mensaje) {
    String result = "";

    //Colecciones individuales que son para los destinatarios
    HashMap<String, Object> sms = new HashMap<>();
    sms.put("phone", numero);
    sms.put("message", mensaje);

    /*HashMap<String, Object> sms2 = new HashMap<>();
    sms2.put("phone", "3188856705");
    sms2.put("message", "Este es un mensaje para Fortox.");*/

    //Colección que contendrá los destinatarios
    ArrayList<HashMap<String, Object>> rows = new ArrayList<>();
    rows.add(sms);
    //rows.add(sms2);

    //Colección principal que contiene la información básica del envío
    Map<String, Object> send = new HashMap<>();
    send.put("name", "Envio desde Java");
    /*
      Si notification es false, el siguiente parametro email se deja como cadena vacia;
      si notification es true, el parametro email contiene 1 o mas correos electronicos (hasta 5)
      separados por coma y sin espacios donde llega notificacion de constancia de cuantos envios fueron realizados.
    */
    send.put("notification", false);
    send.put("email", "");
    send.put("indicative", "57");
    send.put("receiver", rows);
    send.put("idSmsCategory", "43");
    send.put("dateSend", "");
    /*Si datenow es true, el parametro anterior dateSend deja como cadena vacia
      de lo contrario dateSend se coloca fecha y hora con el siguiente formato
      2018-09-21 00:00:00
    */
    send.put("datenow", true);
    send.put("timezone", "-0500");
    
    //Librería Gson que usamos para serializar la coleccion principal en JSON
    Gson gson = new Gson();
    String json = gson.toJson(send);

    result = json;

    return result;
  }
  
  
  
  
  
  
  
  
  

}
